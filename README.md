# xfce-orage caldav

Le but ici étant de synchroniser Orage, le calendrier d'xfce, avec un calendrier caldav, tout en profitant de Lightning, l’extension de Thunderbird, installé par défaut sur la plupart des distributions embarquant Xfce… Et d’avoir d’un coup de clic d’un seul, nos évènements et tâches, comme le permet l’applet calendrier de Gnome…

Pour ce faire nous allons utiliser inotify et systemd en espace utilisateurs… Pour les distributions qui implémentent cette fonctionnalité de systemd ! Ce qui n’est notamment pas le cas pour RedHat, CentOs… Nous parlerons donc ici de la famille Debian, Ubuntu et consorts.

Il nous faut d’abord installer inotify-tools:

`sudo apt install inotify-tools`

Ensuite :
- copier et adapter le fichier “orage_sync” dans /usr/local/bin et le rendre exécutable

`chmod +x /usr/local/bin/orage_sync`
- créer l’arborescence de répertoire systemd/user dans notre répertoire .config

`mkdir -p .config/systemd/user`
- y copier le fichier orage_sync.service
- copier et adapter le fichier .wgetrc à la base de votre répertoire personnel

Ensuite, on active le service :

`systemctl --user enable orage_sync.service`

`systemctl --user start orage_sync.service`

`systemctl --user --system daemon-reload`

Il ne reste plus qu’à dire à Orage d’utiliser comme fichier externe /home/[vous]/.local/share/orage/nextcloud.ics en lecture seule…
